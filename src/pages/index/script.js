import { header } from '../../blocks/header/header';
import modal from '../../blocks/modal/modal';
import { iMaskInit, validationInit } from '../../blocks/form/form';

window.addEventListener('DOMContentLoaded', () => {
  header.init();
  iMaskInit();
  modal();
  validationInit();
});
