import Select from '../../blocks/select/select';
import homeQuizFormInit from '../../blocks/home-quiz/home-quiz';
import Slider from '../../blocks/slider/slider';
import { homeContacts } from '../../blocks/home-contacts/home-contacts';
import homeAdvantagesItems from '../../blocks/home-advantages/home-advantages';

window.addEventListener('DOMContentLoaded', () => {
  homeQuizFormInit();
  homeContacts.init();
  homeAdvantagesItems();
  const selects = document.querySelectorAll('.select .form-input__field');
  selects.forEach((select) => {
    const selectItem = new Select(select, {
      searchEnabled: false,
      itemSelectText: '',
      allowHTML: false,
    });
    selectItem.init();
  });
  const homeClientsSlider = document.querySelector('.home-clients .slider__slides');
  if (homeClientsSlider) {
    const interiorsSliderInstance = new Slider(homeClientsSlider, {
      breakpoints: {
        800: {
          slidesPerView: 4,
        },
        480: {
          slidesPerView: 3,
        },
        320: {
          slidesPerView: 2,
        },
      },
    });
    interiorsSliderInstance.init();
  }
});
