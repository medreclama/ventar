import { isHasSupported } from '../../common/js/modules/helpers';

class Header {
  #element;
  #toggler;
  #activeClass = 'header--active';

  constructor(element) {
    this.#element = element;
    this.#toggler = this.#element.querySelector('.js-header-toggler');
  }

  toggleMenu = () => {
    this.#element.classList.toggle(this.#activeClass);
    if (!isHasSupported()) {
      if (this.#element.classList.contains(this.#activeClass)) document.documentElement.classList.add('header-menu-open');
      else document.documentElement.classList.remove('header-menu-open');
    }
  };

  init = () => {
    this.#toggler.addEventListener('click', this.toggleMenu);
  };
}

export const header = new Header(document.querySelector('.header'));
