const form = document.querySelector('.home-quiz__form');

const homeQuizFormInit = () => {
  if (!form) return;
  const selects = form.querySelectorAll('.home-quiz__select');
  const indicator = document.querySelector('.home-quiz__count');
  const inputsToHide = form.querySelectorAll('.home-quiz__step');
  const STEPS_NUMBER = 6;
  let currentStep = 1;
  // Событие change на селекты
  selects.forEach((select) => {
    select.addEventListener('change', (evt) => {
      const selectValue = evt.target.value;
      const parent = evt.target.closest('.home-quiz__step');
      const nextButton = parent.querySelector('.home-quiz__button--next');
      if (selectValue === '') {
        nextButton.disabled = true;
        nextButton.classList.add('button--disabled');
      } else if (selectValue !== '') {
        nextButton.disabled = false;
        nextButton.classList.remove('button--disabled');
      }
    });
  });

  const goToStep = (stepNumber) => {
    currentStep = stepNumber;
    const inputs = document.querySelectorAll(`.home-quiz__step--${currentStep}`);
    inputsToHide.forEach((inputToHide) => {
      inputToHide.classList.add('home-quiz__step--hidden');
    });

    inputs.forEach((input) => {
      input.classList.remove('home-quiz__step--hidden');
    });
  };

  const goNext = () => {
    currentStep += 1;
    goToStep(currentStep);
    indicator.innerHTML = `${currentStep}/${STEPS_NUMBER - 1}`;
  };

  const goPrevious = () => {
    currentStep -= 1;
    goToStep(currentStep);
    indicator.innerHTML = `${currentStep}/${STEPS_NUMBER - 1}`;
  };

  form.addEventListener('click', (evt) => {
    if (evt.target.classList.contains('home-quiz__button--previous')) goPrevious();
    else if (evt.target.classList.contains('home-quiz__button--next') || evt.target.classList.contains('home-quiz__button--skip')) goNext();
    if (currentStep === STEPS_NUMBER) {
      indicator.innerHTML = '';
      document.querySelector('.home-quiz__header h2').innerHTML = 'Спасибо! Укажите ваши контакты';
      document.querySelector('.home-quiz__text').innerHTML = 'В ближайшее время мы пришлем вам рекомендуемые товары на почту';
    } else {
      document.querySelector('.home-quiz__header h2').innerHTML = 'Не знаете какой выбрать?';
      document.querySelector('.home-quiz__text').innerHTML = 'Ответьте на несколько вопросов и получите персональные рекомендации';
    }
  });
};

export default homeQuizFormInit;
