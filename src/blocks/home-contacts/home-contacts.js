/* eslint-disable max-len */
import media from '../match-media/match-media';

const initMap = () => {
  const map = document.getElementById('home-contacts-map');
  if (!map) return;
  const iconSize = [41, 75];
  const iconOffset = [-20, -50];

  // eslint-disable-next-line no-undef
  const myMap = new ymaps.Map(map, {
    center: [55.6, 37.339],
    controls: ['zoomControl'],
    zoom: 10,
  }, {
    searchControlProvider: 'yandex#search',
    autoFitToViewport: 'always',
  });
  myMap.behaviors.disable('scrollZoom');
  // myMap.behaviors.disable('drag');
  myMap.behaviors.disable('multiTouch');
  const balloon1 = 'г. Москва, Ткацкая, 1';
  const balloon2 = 'г. Подольск, Лобачёва, 32';

  // eslint-disable-next-line no-undef
  const placemark1 = new ymaps.Placemark(
    [55.7864, 37.7188],
    {
      balloonContent: balloon1,
      iconCaption: balloon1,
    },
    {
      iconLayout: 'default#image',
      iconImageHref: 'themes/steamtraps/images/pin.svg', // Своё изображение иконки метки
      // iconImageHref: '/bitrix/templates/tpl_medall/images/pin.svg', // Своё изображение иконки метки
      iconImageSize: iconSize, // Размеры метки
      iconImageOffset: iconOffset, // Смещение левого верхнего угла иконки относительно её "ножки" (точки привязки).
    },
  );

  // eslint-disable-next-line no-undef
  const placemark2 = new ymaps.Placemark(
    [55.44450816369154, 37.57465978778037],
    {
      balloonContent: balloon2,
      iconCaption: balloon2,
    },
    {
      iconLayout: 'default#image',
      iconImageHref: 'themes/steamtraps/images/pin.svg', // Своё изображение иконки метки
      iconImageSize: iconSize, // Размеры метки
      iconImageOffset: iconOffset, // Смещение левого верхнего угла иконки относительно её "ножки" (точки привязки).
    },
  );

  myMap.geoObjects.add(placemark1);
  myMap.geoObjects.add(placemark2);

  const matchMediaMap = () => {
    media.addChangeListener('max', 'm', matchMediaMap);
    if (media.max('m')) myMap.setCenter([55.62, 37.617671]);
    else myMap.setCenter([55.6, 37.339]);
  };

  matchMediaMap();
};

class HomeContacts {
  #element;
  #isYandexMapShowed;

  constructor(element) {
    this.#element = element;
  }

  #scrollHandler = () => {
    if (!this.#isYandexMapShowed) {
      // eslint-disable-next-line no-undef
      ymaps.ready(initMap);
      this.#isYandexMapShowed = true;
      window.removeEventListener('scroll', this.#scrollHandler);
    }
  };

  init = () => window.addEventListener('scroll', this.#scrollHandler);
}

export const homeContacts = new HomeContacts(document.querySelector('.home-contacts'));
