const observer = new IntersectionObserver((entries) => {
  entries.forEach((entry) => {
    if (entry.isIntersecting) {
      entry.target.classList.add('home-advantages__item--active');
    }
  });
});

const items = document.querySelectorAll('.home-advantages__item');
const homeAdvantagesItems = () => {
  items.forEach((item) => observer.observe(item));
};

export default homeAdvantagesItems;
