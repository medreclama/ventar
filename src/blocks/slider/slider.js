import { Swiper } from 'swiper';
import { Navigation } from 'swiper/modules';

class Slider {
  #slider;
  #elem;
  #modules = [Navigation];
  #recievedSettings;

  #settings = {
    modules: this.#modules,
    navigation: {
      nextEl: '.slider__arrow--next',
      prevEl: '.slider__arrow--prev',
      disabledClass: 'slider__arrow--disabled',
    },
  };

  constructor(elem, settings = {}) {
    this.#elem = elem;
    this.#recievedSettings = settings;
    Object.assign(this.#settings, this.#recievedSettings);
  }

  init() {
    this.#slider = new Swiper(this.#elem, this.#settings);
  }
}

export default Slider;
