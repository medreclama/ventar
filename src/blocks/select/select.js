import Choices from 'choices.js';

class Select {
  #select;
  #element;
  #recievedSettings;

  constructor(element, settings = {}) {
    this.#element = element;
    this.#recievedSettings = settings;
  }

  init() {
    this.#select = new Choices(this.#element, this.#recievedSettings);
  }
}

export default Select;
