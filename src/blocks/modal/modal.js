const html = document.querySelector('html');

export const closeModal = (element) => {
  element.classList.remove('modal--active');
  html.style.removeProperty('overflow');

  const closeEvent = new Event('modalClose');
  element.dispatchEvent(closeEvent);
};

export const closeOnEsc = (event) => {
  if (event.code === 'Escape' || event.code === 'Esc') {
    const elements = document.querySelectorAll('.modal--active');
    event.preventDefault();
    Array.from(elements).forEach((element) => closeModal(element));
    window.removeEventListener('keydown', closeOnEsc);
  }
};

const toSnakeCase = (str) => {
  const result = str.split('').map((letter, idx) => {
    const item = letter.toUpperCase() === letter ? `${idx !== 0 ? '_' : ''}${letter.toLowerCase()}` : letter;
    return item;
  }).join('');
  return result;
};

export const showModal = (element, openerDataSet) => {
  element.classList.add('modal--active');
  html.style.setProperty('overflow', 'hidden');

  element.addEventListener('click', (evt) => {
    if (evt.target.classList.contains('modal') || evt.target.classList.contains('modal__close')) closeModal(element);
  });

  window.addEventListener('keydown', closeOnEsc);

  // Если на кнопке открывающей форму были data-set данные,
  // передадим эти значения в соответствующие поля на форме
  const form = element.querySelector('form');
  if (form) {
    Object.entries(openerDataSet).forEach((entry) => {
      const [key, value] = entry;
      const field = toSnakeCase(key);
      if (form[field]) {
        form[field].value = value;
      }
    });
  }
};

const modal = () => {
  const openers = document.querySelectorAll('[data-modal]');
  Array.from(openers).forEach((opener) => {
    opener.addEventListener('click', (event) => {
      event.preventDefault();
      const href = event.target.getAttribute('href');
      if (href.indexOf('#') !== 0) return;
      const id = href.slice(1);
      const element = document.getElementById(id);
      if (!element) return;
      showModal(element, event.target.dataset);
    });
  });
};

export default modal;
