import IMask from 'imask';
import JustValidate from 'just-validate';
import { showModal } from '../modal/modal';

export const iMaskInit = () => {
  const telFields = document.querySelectorAll('input[type="tel"]');
  const maskOptions = {
    mask: '+{7}(900) 000-00-00',
  };
  telFields.forEach((field) => new IMask(field, maskOptions));
};

const telValidation = [
  {
    rule: 'required',
    errorMessage: 'Введите номер телефона',
  },
  {
    rule: 'minLength',
    value: 17,
    errorMessage: 'Введите номер телефона из 11 цифр',
  },
];

const emailValidation = [
  {
    rule: 'required',
    errorMessage: 'Введите e-mail',
  },
  {
    rule: 'email',
    errorMessage: 'Введите правильный e-mail',
  },
];

const textValidation = [
  {
    rule: 'required',
    errorMessage: 'Обязательное поле',
  },
];

const oneFieldValidation = [
  {
    rule: 'required',
    errorMessage: 'Заполните одно из полей',
  },
];

const showMessage = (text) => {
  const message = document.createElement('div');
  message.className = 'modal';
  message.innerHTML = `
    <div class="modal__inner">
      <p>${text}</p>
      <button type="button" class="modal__close"></button>
    </div>
    `;
  document.body.append(message);
  showModal(message);
  message.addEventListener('modalClose', () => message.remove());
};

const sendData = (onSuccess, onFail, body) => {
  fetch('https://26.javascript.pages.academy/kekstagram', { // Тестовый сервер для ajax
    method: 'POST',
    body,
  })
    .then((response) => {
      if (response.status) { // вместо response.ok для теста
        response.json();
        onSuccess();
      } else onFail();
    })
    .catch(() => onFail());
};

const submitForm = (form, successMessage = 'Спасибо! В ближайшее время наш менеджер свяжется с вами') => {
  const onSuccess = () => showMessage(successMessage);
  const onFail = () => showMessage('Произошла ошибка, перезагрузите страницу');
  sendData(
    () => {
      onSuccess();
      form.reset();
    },
    () => {
      onFail();
    },
    new FormData(form),
  );
};

const formCallback = document.querySelector('.form--callback');
const formQuiz = document.querySelector('.form--quiz');
const formRequest = document.querySelector('.form--request');
const formOffer = document.querySelector('.form--offer');
const formOrder = document.querySelector('.form--order');
const formCoefficient = document.querySelector('.form--coefficient');

export const validationInit = () => {
  if (formCallback) {
    const callbackFormValidation = new JustValidate(formCallback);
    callbackFormValidation.addField('input[type="tel"]', telValidation).onSuccess(() => submitForm(formCallback));
  } // Форма заказать звонок

  if (formCoefficient) {
    const coefficientFormValidation = new JustValidate(formCoefficient); // Форма с коэффициентом
    coefficientFormValidation.addField('input[type="number"]', textValidation).onSuccess(() => submitForm(formCoefficient, 'Ваши данные приняты'));
  }

  if (!formQuiz || !formRequest) return;
  const quizFormValidation = new JustValidate(formQuiz); // Форма квиза
  const requestFormValidation = new JustValidate(formRequest); // Форма оставить заявку
  const offerFormValidation = new JustValidate(formOffer); // ЗАПРОСИТЬ КОММЕРЧЕСКОЕ ПРЕДЛОЖЕНИЕ
  const orderFormValidation = new JustValidate(formOrder); // Форма Заказать в товарах

  quizFormValidation.addField('input[type="tel"]', telValidation).addField('input[type="email"]', emailValidation).onSuccess(() => submitForm(formQuiz));
  requestFormValidation.addField('input[type="text"]', textValidation).addField('input[type="tel"]', telValidation).onSuccess(() => submitForm(formRequest));
  orderFormValidation.addField('input[type="text"]', textValidation).addField('input[type="email"]', emailValidation).addField('input[type="tel"]', telValidation).onSuccess(() => submitForm(formOrder, 'Спасибо за заказ! <br> В течении часа (в рабочее время с 9 до 18) мы свяжемся с вами для уточнения деталей по способу получения и оплаты'));

  // Условия для валидации одного из полей
  offerFormValidation.addField('input[type="tel"]', oneFieldValidation).addField('input[type="email"]', oneFieldValidation).onSuccess(() => submitForm(formOffer));
  formOffer.addEventListener('input', (evt) => {
    if (evt.target.type === 'tel' && evt.target.value !== '') {
      offerFormValidation.addField('input[type="tel"]', telValidation);
      offerFormValidation.removeField('input[type="email"]', emailValidation);
    } else if (evt.target.type === 'email' && evt.target.value !== '') {
      offerFormValidation.addField('input[type="email"]', emailValidation);
      offerFormValidation.removeField('input[type="tel"]', telValidation);
    }
  });
};
